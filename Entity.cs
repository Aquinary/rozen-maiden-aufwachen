﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Mine2D
{
    class Entity
    {
        // Текстура
        Texture2D texture;
        // Размеры
        public int W = 24;
        public int H = 48;
        // Положение
        public float X = 0;
        public float Y = 0;
        // Ускорение 
        public float DX = 0.0f;
        public float DY = 0.0f;
        // Скорость
        public float speed;
        // Сила прыжка
        public float J = -0.6f;
        // Сила гравитации
        public float G = 0.0065f;
        // Нахождение в воздухе
        public bool jump = false;

        public Entity(Microsoft.Xna.Framework.Content.ContentManager content, string path_to_texture)
        {
            // Устанавливаем основные свойства
            texture = content.Load<Texture2D>(path_to_texture);
        }

        public void SetPositionX (float x)
        {
            // Установка позиции X
            this.X = x;
        }

        public void SetPositionY(float y)
        {
            // Установка позиции Y
            this.Y = y;
        } 

        public float GetPositionX()
        {
            // Возврат позиции по X
            return this.X;
        }

        public float GetPositionY()
        {
            // Возврат позиции по Y
            return this.Y;
        }

        public void SetSize(int w, int h)
        {
            // Установка размеров
            this.W = w;
            this.H = h;
        }

        public int GetSizeW()
        {
            // Возврат размера по W
            return this.W;
        }

        public int GetSizeH()
        {
            // Возврат размера по H
            return this.H;
        }

        public void SetDirectionX(float dx)
        {
            // Установка ускорения по X
            this.DX = dx;
        }

        public void SetDirectionY(float dy)
        {
            // Установка ускорения по Y
            this.DY = dy;
        }

        public float GetDirectionX()
        {
            // Возврат ускорения по X
            return this.DX;
        }

        public void SetSpeed(float speed)
        {
            // Установка скорости
            this.speed = speed;
        }

        public float GetSpeed()
        {
            // Возврат скорости
            return this.speed;
        }


        public float GetDirectionY()
        {
            // Возврат ускорения по Y
            return this.DY;
        }

        public void SetJumpingForce(float force)
        {
            // Установка силы прыжка
            this.J = force;
        }

        public float GetJumpingForce()
        {
            // Возврат силы прыжка
            return this.J;
        }

        public void SetGravityForce(float gravity)
        {
            // Установка силы гравитации
            this.G = gravity;
        }

        public float GetGravityForce()
        {
            // Возврат силы гравитации
            return this.G;
        }

        public Texture2D GetTexture()
        {
            // Возврат текстуры
            return this.texture;
        }

        public void SetJump(bool enable)
        {
            // Устанавливаем статус прыжка активным
            this.jump = enable;
        }

        public bool GetJump()
        {
            // Возвращаем статус прыжка
            return this.jump;
        }
    }
}
