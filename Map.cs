﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Mine2D
{
    class Map
    {
        // Размеры мира
        List<List<int>> map = new List<List<int>>();

        // Размер мира по X
        private int world_size_x;
        // Размер мира по Y
        private int world_size_y;

        public Map(int world_size_x, int world_size_y)
        {
            // Устанавливаем размер мира
            this.world_size_x = world_size_x;
            this.world_size_y = world_size_y;
           
            for (int x = 0; x < world_size_x; x++)
            {
                for (int y = 0; y < world_size_y; y++)
                {
                    // Обнуляем массив мира
                    map.Add(new List<int>());
                    map[x].Add(0);
                    map[x].Insert(y, 0); 
                }
            }
        }

        public int GetWorldSizeX()
        {
            // Получаем размер мира по X
            return this.world_size_x;
        }

        public int GetWorldSizeY()
        {
            // Получаем размер мира по Y
            return this.world_size_y;
        }

        public void SetBlock(int x, int y, int block)
        {
            //Устанавливаем блок по позиции
            this.map[x].RemoveAt(y);
            this.map[x].Insert(y, block);
           
        }

        public int GetBlock(int x, int y)
        {
            // Возвращаем блок по позиции
            return (int)this.map[x][y]; 
        }


        public void GenerateFlat()
        {
            // Генерация ровной поверхности
            for (int x = 0; x < world_size_x; x++)
            {
                for (int y = 0; y < world_size_y; y++)
                {
                    // Если высота равна половине...
                    if (y == world_size_y / 2 - 1 )
                    {
                        // ...ставим блок травы
                        this.map[x].Insert(y, (int)Block.Type.Grass);
                    }
                    // Если высота ниже уровня травы...
                    if (y > world_size_y / 2 - 1)
                    {
                        // ...ставим блок грязи
                        this.map[x].Insert(y, (int)Block.Type.Dirt);
                    }
                }
            }
            // Генерация стенок
            GenerateWall();
        }

        public void GenerateWall()
        {
            // Генерация стенок
            for (int x = 0; x < world_size_x; x++)
            {
                for (int y = 0; y < world_size_y; y++)
                {
                    if (x == 0 || x == world_size_x - 1 || y == 0 || y == world_size_y - 1)
                    {
                        this.map[x][y] = (int)Block.Type.BedRock;
                    }
                }
            }
        }
        
        public void GenerateForest()
        {
            // Уровень нуля
            int level_null = world_size_y / 2;
            // Максимальное смещение вверх
            int level_up = -(20 * level_null) / 100;
            // Максимальное смещение вниз
            int level_down = (20 * level_null) / 100;
            // Степень смещения
            int off = 0;
            // Количество грязи (в %)
            int max_dirt = 10;
            int min_dirt = 5;
            // Переменные для генерации 
            Random offset = new Random(Main.seed);
            Random direction = new Random(Main.seed);
            // Генерация первой (основной) линии ландшафта
            for (int x = 0; x < world_size_x; x++)
            {
                // Генерируем число
                switch (direction.Next(0, 3))
                {
                    case 0:
                        //Смещение вниз
                        off += offset.Next(0, 2);
                        break;
                    case 1:
                        //Смещение вверх
                        off -= offset.Next(0, 2);
                        break;
                    case 2:
                        //Отсутствие смещения
                        off += 0;
                        break;
                    default:

                        break;
                }
                // Если смещение больше допустимого, то исправляем
                if (off > level_down) { off -= 1; }
                if (off < level_up) { off += 1; }

                for (int y = 0; y < world_size_y; y++)
                {

                    if (y == world_size_y / 2)
                    {
                        // Вывод результата
                        map[x][y + off] = (int)Block.Type.Grass;
                    }
                }
            }
            // Генерация почвы под линией и камня
            for (int x = 0; x < world_size_x; x++)
            {
                for (int y = 0; y < world_size_y; y++)
                {
                    // Ищем блок, смотрим, что под ним, после чего забиваем нужным блоком
                    if (map[x][y] == (int)Block.Type.Grass && map[x][y + 1] == (int)Block.Type.None)
                    {
                        for (int i = world_size_y; i > 0; i--)
                        {
                            // Генерируем грязь
                            map[x][y + i] = (int)Block.Type.Dirt;
                        }
                        Random rnd = new Random();
                        for (int i = world_size_y; i > rnd.Next((min_dirt * world_size_y / 2) / 100, (max_dirt * world_size_y / 2) / 100); i--)
                        {
                            //Генерируем камни
                            map[x][y + i] = (int)Block.Type.Stone;
                        }
                    }
                }
            }
            // Генерация стенок
            GenerateWall();
        }

        public void GenerateSmooth()
        {
            for (int x = 0; x < world_size_x; x++)
            {
                for (int y = 0; y < world_size_y; y++)
                {
                    // Убираем выступающие одиночные блоки
                    if (map[x][y] == (int)Block.Type.Grass && map[x+1][y] == (int)Block.Type.None && map[x-1][y] == (int)Block.Type.None && map[x][y-1] == (int)Block.Type.None)
                    {
                        map[x][y] = (int)Block.Type.None;
                    }

                    // Добавляем блоки в места, где углубление в один блок шириной
                    if (map[x][y] == (int)Block.Type.None && map[x + 1][y] == (int)Block.Type.Grass && map[x - 1][y] == (int)Block.Type.Grass && map[x][y + 1] == (int)Block.Type.Grass && map[x][y - 1] == (int)Block.Type.None)
                    {
                        map[x][y] = (int)Block.Type.Grass;
                    }
                }
            }
            // Генерация стенок
            GenerateWall();
        }

        public  void DrawMap(GameWindow window, SpriteBatch spriteBatch, Camera camera, Block[] block, bool enable)
        {

            Color color = new Color();
            if (!enable)
            {
                // Фоновый мир
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
                color.A = 255;
                color.R = 155;
                color.G = 155;
                color.B = 155;
            }
            else
            {
                // Основной мир
                spriteBatch.Begin();
                color.A = 255;
                color.R = 255;
                color.G = 255;
                color.B = 255;
            }

            for (int x = (int)camera.GetPositionX() / Setting.GetTileSizeH(); x < (int)camera.GetPositionX() / Setting.GetTileSizeH() + window.ClientBounds.Width / Setting.GetTileSizeH() + Setting.GetTileSizeH(); x++)
            {
                for (int y = (int)camera.GetPositionY() / Setting.GetTileSizeH(); y < (int)camera.GetPositionY() / Setting.GetTileSizeH() + window.ClientBounds.Height / Setting.GetTileSizeH() + Setting.GetTileSizeH(); y++)
                {
                    // Не рисуем то, что находится вне границ мира
                    if ((x >= 0 && y >= 0) && (x < this.world_size_x && y < this.world_size_y))
                    {
                        // Вывод мира
                        spriteBatch.Draw(block[map[x][y]].GetTexture(), new Vector2(x * Setting.GetTileSizeW() - camera.GetPositionX(), y * Setting.GetTileSizeH() - camera.GetPositionY()), null, color, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 0);
                    }
                }
            }
            spriteBatch.End();
        }
    }
}
