﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Mine2D
{
    class Block
    {
        public enum Type : int
        {
            // Список блоков
            None = 0,
            BedRock = 1,
            Dirt = 2,
            Grass = 3,
            Stone = 4,
            CobbleStone = 5,
            MossStone = 6,
            WoodOak = 7,
            WoodBirch = 8,
            WoodenPlanks = 9,
            Leaves = 10,
            CoalOre = 11,
            IronOre = 12,
            RedStoneOre = 13,
            GoldOre = 14,
            LapisOre = 15,
            DiamondOre = 16,
            IronBlock = 17,
            GoldBlock = 18,
            LapisBlock = 19,
            DiamondBlock = 20,
            Mycelium = 21,
            ShowDirt = 22,
            Ice = 23,
            Sand = 24,
            Gravel = 25,
            SoulSand = 26
        } 

        //Временная переменная под размеры тайлов
        static int h = 64;
        static int w = 64;
        // Текстура под блок
        Texture2D texture;
       
        // Размеры блока
        private int W;
        private int H;
        
        // Номер блока под колесико
        static int num_block = 2;
            
        public Block(Microsoft.Xna.Framework.Content.ContentManager content, string path_to_texture)
        {
            // Ставим текстуру
            this.texture = content.Load<Texture2D>(path_to_texture);
        }

        public void SetSize(int w, int h)
        {
            // Устанавливаем размеры
            this.W = w;
            this.H = h;
        }

        public Vector2 GetSize()
        {
            // Возвращаем Размеры
            return new Vector2(this.W, this.H);
        }

        public Texture2D GetTexture()
        {
            // Возвращаем текстуру
            return this.texture;
        }

        public static void NextBlock()
        {
            // Получаем следующий блок
            // Проверяем диапазон
            if (num_block < Enum.GetValues(typeof(Block.Type)).Length-1)
            {
                num_block += 1;
            }
        }

        public static void PreviosBlock()
        {
            // Получаем предыдущий блок
            // Проверяем диапазон
            if (num_block > 2)
            {
                num_block -= 1;
            }
        }

        public static int GetCurrentBlock()
        {
            // Получает текущий блок
            return num_block;
        }

        public static int GetCountBlock()
        {
            // Получаем количество блоков, основываясь на списке
            return Enum.GetValues(typeof(Block.Type)).Length;
        }
    }
}
