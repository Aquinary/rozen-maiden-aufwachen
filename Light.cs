﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;
namespace Mine2D
{
    class Light
    {
        Texture2D texture;
        public List<List<int>> light_map = new List<List<int>>();

        //Размеры карты освещения
        public int light_size_x = 0;
        public int light_size_y = 0;
        public Light(GameWindow window, Microsoft.Xna.Framework.Content.ContentManager content, string path_to_texture, int width, int height)
        {
            // Ставим текстуру
            this.texture = content.Load<Texture2D>(path_to_texture);
            // Размеры карты освещения
            this.light_size_x = width;
            this.light_size_y = height;
            for (int x = 0; x < light_size_x; x++)
            {
                for (int y = 0; y < light_size_y; y++)
                {
                    // Обнуляем массив мира
                    light_map.Add(new List<int>());
                    light_map[x].Add(200);
                    light_map[x].Insert(y, 200);
                }
            }
        }

        public Texture2D GetTexture()
        {
            // Возвращаем текстуру
            return this.texture;
        }

        public void GenerateSphere(int x, int y, int r)
        {
            int x1;
            int x2;
            int y1;
            int y2;
            x2 = x + r;
            y2 = y;
            for (int c = 0; c <= r; c++)
            {
                for (int a = 1; a <= 360; a++)
                {

                    x1 = x2; y1 = y2;
                    x2 = (int)Math.Round(c * Math.Cos(a)) + x;
                    y2 = (int)Math.Round(c * Math.Sin(a)) + y;
                    if (light_map[x2][y2] < 255)
                    {
                        light_map[x2][y2] = c * 38;
                    }
                   
                }
                
            }
        }
        public void GenerateNull()
        {
            for (int x = 0; x < light_size_x; x++)
            {
               for (int y = 0; y < light_size_y; y++)
                {
                    light_map[x][y] = 200;
                }
            }
        }
    }
}
