﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mine2D
{
    class Collision
    {
        Entity entity;
        public Collision()
        {
           
        }

        public void Physics(Entity entity, Map world, float time)
        {
            entity.SetJump(true);
            // Обработка коллизий игрока

            if (entity.GetJump())
            {
                // Гравитация
                entity.SetDirectionY(entity.GetDirectionY() + entity.GetGravityForce() * time);
            }

            // Финальная позиция игрока
            entity.SetPositionX(entity.GetPositionX() + entity.GetDirectionX() * time);
            Collisions(entity, world, false);
            entity.SetPositionY(entity.GetPositionY() + entity.GetDirectionY() * time);
            Collisions(entity, world, true);
            
            // Обнуляем ускорение
            entity.SetDirectionX(0);
        }

        public void Collisions(Entity entity, Map world, bool dir)
        {
            if (entity.GetDirectionY() > world.GetWorldSizeY()-1)
            {
                entity.SetDirectionY(0);
                entity.SetJump(false);
            }
            // Проверяем блоки непосредственно находящиеся рядом с и гроком
            
            for (int X = (int)entity.GetPositionX() / Setting.GetTileSizeW(); X < (entity.GetPositionX() + entity.GetSizeW()) / Setting.GetTileSizeW(); X++)
            {
                for (int Y = (int)entity.GetPositionY() / Setting.GetTileSizeW(); Y < (entity.GetPositionY() + entity.GetSizeH()) / Setting.GetTileSizeW(); Y++)
                {
                    // Проверям, находится ли игрок в мире
                    if ((X / Setting.GetTileSizeW() >= 0 && Y / Setting.GetTileSizeW() >= 0) && (X / Setting.GetTileSizeW() < world.GetWorldSizeX() && Y / Setting.GetTileSizeW() < world.GetWorldSizeY()))
                    {
                        //Не взаимодействуем с пустым блоком
                        if (world.GetBlock(X, Y) != (int)Block.Type.None)
                        {
                            //Обрабатываем ось X
                            if (entity.GetDirectionX() > 0 && dir == false)
                            {
                                entity.SetPositionX(X * Setting.GetTileSizeW() - entity.GetSizeW());
                            }
                            if (entity.GetDirectionX() < 0 && dir == false)
                            {
                                entity.SetPositionX(X * Setting.GetTileSizeW() + Setting.GetTileSizeW());
                            }
                            //Обрабатываем ось Y
                            if (entity.GetDirectionY() > 0 && dir == true)
                            {
                                entity.SetPositionY(Y * Setting.GetTileSizeW() - entity.GetSizeH());
                                entity.SetDirectionY(0);
                                entity.SetJump(false);
                            }
                            if (entity.GetDirectionY() < 0 && dir == true)
                            {
                                entity.SetPositionY(Y * Setting.GetTileSizeW() + Setting.GetTileSizeW());
                                entity.SetDirectionY(0);
                            }
                        }
                    }
                }
            }
        }
    }
}
