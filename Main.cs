﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections;

namespace Mine2D
{
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteBatch spriteBatch2;
        // Таймер для стабилизации FPS
        Timer timer;
        // Массив под миры
        Map[] world = new Map[Setting.GetWorldCount()]; // Основной мир
        Map[] back_world = new Map[Setting.GetWorldCount()]; // Мир под задний фон

        //Массив под блоки
        Block[] block = new Block[Setting.GetBlockCount()];
        // Игрок
        Entity player;
        // Коллижен для сущности
        Collision entity;
        // Камера
        Camera camera;
        // Элемент GUI 'выбор'
        GUI select;
        // Освещение
        Light light;
        // Переменная под сид
        public static int seed = 0;
        // Временные переменные под статус колеса мыши
        float prevWheelValue;
        float currWheelValue;
        // 
        bool background = false;
        RenderTarget2D rend;
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            // Размеры приложения
            graphics.PreferredBackBufferWidth = 1600;
            graphics.PreferredBackBufferHeight = 1000;
        }

        protected override void Initialize()
        {
            
            //Поле эксперементов
          
            Random rnd = new Random();
            seed = rnd.Next(0, 1000);
            // Инициализация таймера
            timer = new Timer();
            // Инициализация коллижина
            entity = new Collision();
            // Инициализация камеры
            camera = new Camera();
            // Инициализация GUI
            select = new GUI(Content, "GUI\\select.png");
            // Определение размеров мира
            world[0] = new Map(100, 51);
            world[0].GenerateForest();
            world[0].GenerateSmooth();
            back_world[0] = new Map(100, 51);
            back_world[0].GenerateForest();
            
            // Создание игрока
            player = new Entity(Content, "player\\body.png");
            // Центрировка игрока
            player.SetPositionX(world[0].GetWorldSizeX() / 2 * player.GetSizeW());
            player.SetPositionY(world[0].GetWorldSizeY() / 2 * player.GetSizeH() - 1000);
            player.SetJumpingForce(-0.8f);
            player.SetGravityForce(0.0013f);
            player.SetSpeed(0.5f);
            
            this.IsMouseVisible = true;
            base.Initialize();
            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatch2 = new SpriteBatch(GraphicsDevice);
            //Загрузка ресурсов
            block[(int)Block.Type.None] = new Block(Content, "block\\void.png");
            block[(int)Block.Type.BedRock] = new Block(Content, "block\\bedrock.png");
            block[(int)Block.Type.Dirt] = new Block(Content, "block\\dirt.png");
            block[(int)Block.Type.Grass] = new Block(Content, "block\\grass.png");
            block[(int)Block.Type.Stone] = new Block(Content, "block\\stone.png");
            block[(int)Block.Type.CobbleStone] = new Block(Content, "block\\cobblestone.png");
            block[(int)Block.Type.MossStone] = new Block(Content, "block\\moss_stone.png");
            block[(int)Block.Type.WoodOak] = new Block(Content, "block\\wood_oak.png");
            block[(int)Block.Type.WoodBirch] = new Block(Content, "block\\wood_birch.png");
            block[(int)Block.Type.WoodenPlanks] = new Block(Content, "block\\wooden_planks.png");
            block[(int)Block.Type.Leaves] = new Block(Content, "block\\leaves.png");
            block[(int)Block.Type.CoalOre] = new Block(Content, "block\\coal_ore.png");
            block[(int)Block.Type.IronOre] = new Block(Content, "block\\iron_ore.png");
            block[(int)Block.Type.RedStoneOre] = new Block(Content, "block\\redstone_ore.png");
            block[(int)Block.Type.GoldOre] = new Block(Content, "block\\gold_ore.png");
            block[(int)Block.Type.LapisOre] = new Block(Content, "block\\lapis_ore.png");
            block[(int)Block.Type.DiamondOre] = new Block(Content, "block\\diamond_ore.png");
            block[(int)Block.Type.IronBlock] = new Block(Content, "block\\iron_block.png");
            block[(int)Block.Type.GoldBlock] = new Block(Content, "block\\gold_block.png");
            block[(int)Block.Type.LapisBlock] = new Block(Content, "block\\lapis_block.png");
            block[(int)Block.Type.DiamondBlock] = new Block(Content, "block\\diamond_block.png");
            block[(int)Block.Type.Mycelium] = new Block(Content, "block\\mycelium.png");
            block[(int)Block.Type.ShowDirt] = new Block(Content, "block\\snowdirt.png");
            block[(int)Block.Type.Ice] = new Block(Content, "block\\ice.png");
            block[(int)Block.Type.Sand] = new Block(Content, "block\\sand.png");
            block[(int)Block.Type.Gravel] = new Block(Content, "block\\gravel.png");
            block[(int)Block.Type.SoulSand] = new Block(Content, "block\\soul_sand.png");
            light = new Light(Window, Content, "light\\1.png", world[0].GetWorldSizeX(), world[0].GetWorldSizeY());
            rend = new RenderTarget2D(GraphicsDevice, 500, 600);
        }

        protected override void UnloadContent()
        {
            
        }
       
        protected override void Update(GameTime gameTime)
        {
            // Регулятор FPS
            float time = timer.GetMilliseconds();
            timer.Reset();
            // Прослушка состояний клавиатуры и мыши
            KeyboardState key = new KeyboardState();
            MouseState mouse = new MouseState();
            // Получение состояний
            key = Keyboard.GetState();
            mouse = Mouse.GetState();
            // Обработка состояний
            if (key.IsKeyDown(Keys.D))
            {
                player.SetDirectionX(player.GetSpeed());
            }
            if (key.IsKeyDown(Keys.A))
            {
                player.SetDirectionX(-player.GetSpeed());
            }
            if (key.IsKeyDown(Keys.Space))
            {
                // Если игрок не в воздухе
                if (!player.jump)
                {
                    // Ускоряем его вверх
                    player.SetDirectionY(player.GetJumpingForce());
                    //player.SetJump(true);
                }
            }

           if (key.IsKeyDown(Keys.LeftAlt))
           {
               background = !background;
           }
           if (mouse.LeftButton == ButtonState.Pressed)
            {
                // Получаем координаты мыши
                int x = mouse.X + (int)camera.GetPositionX();
                int y = mouse.Y + (int)camera.GetPositionY();
                // Проверяем ее положение 
                if ((x / Setting.GetTileSizeH() >= 0 && y / Setting.GetTileSizeH() >= 0) && (x / Setting.GetTileSizeH() < world[0].GetWorldSizeX() && y / Setting.GetTileSizeH() < world[0].GetWorldSizeY()))
                {
                    // Если тип не бедрок
                    if (world[0].GetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH())!= (int)Block.Type.BedRock)
                    {
                        //Ставим пустоту
                        world[0].SetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH(), (int)Block.Type.None);
                    }
                }
            }

           if (mouse.RightButton == ButtonState.Pressed)
           {
               if (!background)
               {
                   int x = mouse.X + (int)camera.GetPositionX();
                   int y = mouse.Y + (int)camera.GetPositionY();
                   int left_tile = (int)Math.Floor((player.GetPositionX() + player.GetSizeW() - 1) / Setting.GetTileSizeH());
                   int right_tile = (int)Math.Ceiling((player.GetPositionX() - Setting.GetTileSizeH() + 1) / Setting.GetTileSizeH());
                   int up_tile = (int)Math.Floor((player.GetPositionY() + player.GetSizeH() - 1) / Setting.GetTileSizeH());
                   int down_tile = (int)Math.Ceiling((player.GetPositionY() - Setting.GetTileSizeH() + 1) / Setting.GetTileSizeH());
                   if ((x / Setting.GetTileSizeH() >= 0 && y / Setting.GetTileSizeH() >= 0) && (x / Setting.GetTileSizeH() < world[0].GetWorldSizeX() && y / Setting.GetTileSizeH() < world[0].GetWorldSizeY()))
                   {
                       if (world[0].GetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH()) != (int)Block.Type.BedRock && world[0].GetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH()) == (int)Block.Type.None)
                       {
                           if ((left_tile < x / Setting.GetTileSizeH() || right_tile > x / Setting.GetTileSizeH()) || (up_tile < y / Setting.GetTileSizeH() || down_tile > y / Setting.GetTileSizeH()))
                           {
                               {
                                   world[0].SetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH(), Block.GetCurrentBlock());
                               }
                           }
                       }
                   }
               } else
               {
                   int x = mouse.X + (int)camera.GetPositionX();
                   int y = mouse.Y + (int)camera.GetPositionY();
                   int left_tile = (int)Math.Floor((player.GetPositionX() + player.GetSizeW() - 1) / Setting.GetTileSizeH());
                   int right_tile = (int)Math.Ceiling((player.GetPositionX() - Setting.GetTileSizeH() + 1) / Setting.GetTileSizeH());
                   int up_tile = (int)Math.Floor((player.GetPositionY() + player.GetSizeH() - 1) / Setting.GetTileSizeH());
                   int down_tile = (int)Math.Ceiling((player.GetPositionY() - Setting.GetTileSizeH() + 1) / Setting.GetTileSizeH());
                   if ((x / Setting.GetTileSizeH() >= 0 && y / Setting.GetTileSizeH() >= 0) && (x / Setting.GetTileSizeH() < back_world[0].GetWorldSizeX() && y / Setting.GetTileSizeH() < back_world[0].GetWorldSizeY()))
                   {
                       if (back_world[0].GetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH()) != (int)Block.Type.BedRock && back_world[0].GetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH()) == (int)Block.Type.None)
                       {
                           if ((left_tile < x / Setting.GetTileSizeH() || right_tile > x / Setting.GetTileSizeH()) || (up_tile < y / Setting.GetTileSizeH() || down_tile > y / Setting.GetTileSizeH()))
                           {
                               {
                                   back_world[0].SetBlock(x / Setting.GetTileSizeH(), y / Setting.GetTileSizeH(), Block.GetCurrentBlock());
                               }
                           }
                       }
                   }
               }
           }
            // Обработка мыши
           {
               prevWheelValue = currWheelValue;
               currWheelValue = mouse.ScrollWheelValue;
               if (currWheelValue > prevWheelValue)
               {
                   Block.NextBlock();
               }

               if (currWheelValue < prevWheelValue)
               {
                   Block.PreviosBlock();
               }
           }
            // Физика игрока
            entity.Physics(player, world[0], time);
            // Центрировка камеры по игроку
            camera.SetPositionX(player.GetPositionX() - Window.ClientBounds.Width /2);
            camera.SetPositionY(player.GetPositionY() - Window.ClientBounds.Height / 2);
            // Получение координат GUI select
            select.SetPositionX((mouse.X + (int)camera.GetPositionX()) / Setting.GetTileSizeH() * Setting.GetTileSizeH());
            select.SetPositionY((mouse.Y + (int)camera.GetPositionY()) / Setting.GetTileSizeH() * Setting.GetTileSizeH());
            light.GenerateNull();
            for (int x = 0; x < world[0].GetWorldSizeX(); x++ )
            {
                for (int y = 0; y < world[0].GetWorldSizeY(); y++ )
                {
                    if (world[0].GetBlock(x,y) == (int)Block.Type.CobbleStone)
                    {
                        light.GenerateSphere(x, y, 5);
                    } else
                    {
                        
                    }
                }
            }
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            
            
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
   
            // Вывод миров
            back_world[0].DrawMap(Window, spriteBatch, camera, block,false);
            world[0].DrawMap(Window, spriteBatch, camera, block, true);
            spriteBatch.Begin();
           
            // Вывод игрока
            spriteBatch.Draw(player.GetTexture(), new Vector2(player.GetPositionX() - camera.GetPositionX(), player.GetPositionY() - camera.GetPositionY()), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 1);
            spriteBatch.End();
            spriteBatch.Begin();
            // Вывод GUI
            spriteBatch.Draw(select.GetTexture(), new Vector2(select.GetPositionX() - camera.GetPositionX(), select.GetPositionY() - camera.GetPositionY()), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 1);
            // Вывод выбранного блока
            
            spriteBatch.Draw(block[Block.GetCurrentBlock()].GetTexture(), new Vector2(0, 0), null, Microsoft.Xna.Framework.Color.White, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 1);
            spriteBatch.End();
            spriteBatch2.Begin();
            
            for (int x = (int)camera.GetPositionX() / Setting.GetTileSizeH(); x < (int)camera.GetPositionX() / Setting.GetTileSizeH() + Window.ClientBounds.Width / Setting.GetTileSizeH() + Setting.GetTileSizeH(); x++)
            {
                for (int y = (int)camera.GetPositionY() / Setting.GetTileSizeH(); y < (int)camera.GetPositionY() / Setting.GetTileSizeH() + Window.ClientBounds.Height / Setting.GetTileSizeH() + Setting.GetTileSizeH(); y++)
                {
                    // Не рисуем то, что находится вне границ мира
                    if ((x >= 0 && y >= 0) && (x < world[0].GetWorldSizeX() && y < world[0].GetWorldSizeY()))
                    {
                       
                            Color color = new Color();
                            color.R = 0;
                            color.G = 0;
                            color.B = 255;
                            color.A = (byte)light.light_map[x][y];
                            // Вывод мира
                            //spriteBatch2.Draw(light.GetTexture(), new Vector2(x * Setting.GetTileSizeW() - camera.GetPositionX(), y * Setting.GetTileSizeH() - camera.GetPositionY()), null, color, 0, new Vector2(0, 0), 1f, SpriteEffects.None, 0);
                    }
                }
            }        
            spriteBatch2.End();
            base.Draw(gameTime);
        }
    }
}
