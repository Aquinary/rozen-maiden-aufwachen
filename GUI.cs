﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Mine2D
{
    class GUI
    {
        // Координаты мыши
        int mouse_coord_x;
        int mouse_coord_y;
        // Координаты GUI
        int x;
        int y;
        // Текстура
        Texture2D texture;

        public GUI(Microsoft.Xna.Framework.Content.ContentManager content, string path_to_texture)
        {
            // Установка текстуры
            this.texture = content.Load<Texture2D>(path_to_texture);
        }

        public void SetMouseX(int x)
        {
            // Установка координат X
            this.mouse_coord_x = x;
        }

        public int GetMouseX()
        {
            // Возврат координат X
            return this.mouse_coord_x;
        }

        public void SetMouseY(int y)
        {
            // Установка координат X
            this.mouse_coord_y = y;
        }

        public int GetMouseY()
        {
            // Возврат координат X
            return this.mouse_coord_y;
        }

        public void SetPositionX(int x)
        {
            // Установка координат X
            this.x = x;
        }

        public int GetPositionX()
        {
            // Возврат координат X
            return this.x;
        }

        public void SetPositionY(int y)
        {
            // Установка координат X
            this.y = y;
        }

        public int GetPositionY()
        {
            // Возврат координат X
            return this.y;
        }

        public Texture2D GetTexture()
        {
            return this.texture;
        }
    }
}
