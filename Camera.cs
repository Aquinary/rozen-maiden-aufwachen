﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mine2D
{
    class Camera
    {
        //Позиция камеры
        float X;
        float Y;

        public Camera()
        {

        }

        public void SetPositionX(float x)
        {
            // Установка камеры по X
            this.X = x;
        }

        public float GetPositionX()
        {
            // Получение позиции по X
            return this.X;
        }

        public void SetPositionY(float y)
        {
            // Установка камеры по Y
            this.Y = y;
        }

        public float GetPositionY()
        {
            // Получение позиции по Y
            return this.Y;
        }
    }
}
