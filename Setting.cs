﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Mine2D
{
    static class Setting
    {
        // Количество блоков
        private static int block_count = Block.GetCountBlock();
        // Количество миров
        private static int world_count = 1;
        // Количество сущностей
        private static int entity_count = 1;
        // Cтандартный размер блоков
        private static int tile_size_w = 64;
        private static int tile_size_h = 64;
        public static int GetBlockCount()
        {
            // Получаем количество блоков
            return block_count;
        }

        public static int GetWorldCount()
        {
            // Получаем количество миров
            return world_count;
        }

        public static int GetTileSizeW()
        {
            // Получаем размер тайла по W
            return tile_size_w;
        }

        public static int GetTileSizeH()
        {
            // Получаем размер тайла по W
            return tile_size_h;
        }
    }
}
